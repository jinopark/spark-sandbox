import org.gradle.api.tasks.testing.logging.TestLogEvent

group = "com.jp"
version = "0.0.1-SNAPSHOT"

buildscript {
    repositories {
        mavenLocal()
        mavenCentral()
        jcenter()
    }
}

repositories {
    jcenter()
    mavenCentral()
}

plugins {
    java
    idea
    kotlin("jvm") version "1.3.11"
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

dependencies {
    compile(kotlin("stdlib"))
}
// todo prevent spark jars from being bundled
// todo what's a better way of doing this...
val sparkDeps = setOf(
        "org.apache.spark:spark-core_2.11:2.4.0",
        "org.apache.spark:spark-sql_2.11:2.4.0"
)
val deps = setOf(
        "ch.qos.logback:logback-classic:1.2.3"
)
val testDeps = setOf(
        "junit:junit:4.12"
)
for (d in deps.union(sparkDeps)) {
    dependencies.compile(d)
}
for (d in testDeps) {
    dependencies.testCompile(d)
}

// Configure tasks must come after dependencies block due to
// https://stackoverflow.com/questions/42552511/cannot-change-dependencies-of-configuration-compile-after-it-has-been-resolve
val jar by tasks.getting(Jar::class) {
    // Kotlin is building a class to encapsulate your main function named
    // with the same name of your file - with Title Case.
    manifest {
        attributes["Main-Class"] = "com.jp.SparkSandboxKt"
    }
    // This line of code recursively collects and copies all of a project"s files
    // and adds them to the JAR itself. One can extend this task, to skip certain
    // files or particular types at will
    from(configurations.runtime.map { if (it.isDirectory) it else zipTree(it) })
}

val test by tasks.getting(Test::class) {
    // Always run tests even when not up to date
    outputs.upToDateWhen { false }

    // Groovy takes an untyped closure; sigh.
    beforeTest(closureOf<Any> { logger.lifecycle("Running test: $this") })

    testLogging {
        events = setOf(
                TestLogEvent.PASSED,
                TestLogEvent.SKIPPED,
                TestLogEvent.FAILED,
                TestLogEvent.STANDARD_OUT,
                TestLogEvent.STANDARD_ERROR
        )

        // This is just for standard streams
        // showStandardStreams = true
    }
}

val wrapper by tasks.getting(Wrapper::class) {
    // Stop changing distribution type
    distributionType = Wrapper.DistributionType.ALL
}

jar.dependsOn(test)

