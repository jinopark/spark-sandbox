package com.jp

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col
import org.junit.Test
import java.io.File

class SparkSandboxTest {

    private val spark = SparkSession
            .builder()
            .master("local")
            .appName("Test")
            .orCreate

    @Test
    fun `test simple filter operation`() {

        val input = File(SparkSandboxTest::class.java.getResource("/data/people.json").toURI())
        var df = spark
                .read()
                .json(input.absolutePath)

        df = df.filter(col("name").like("%n%"))
        df.show()
    }

    @Test
    fun `test tsv operation`() {

        val input = File(SparkSandboxTest::class.java.getResource("/data/test_data.tsv").toURI())
        var df = spark
                .read()
                .option("sep", "\t")
                .option("inferSchema", "true")
                .option("header", "false")
                .csv(input.absolutePath)

        df = df.orderBy(col("_c0"))
        df.show(10)
    }
}